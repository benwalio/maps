import mapboxgl from 'mapbox-gl';

mapboxgl.accessToken =
  'pk.eyJ1IjoiYmFyZWZvb3R5ZXRpcmFtYmxlciIsImEiOiJja25kNDAzbnoxczA5MnF0YXc5MzU1MHY1In0.p8qd2rz_Sdq8FBJpx6kVpA';
  
export interface Mappable {
  location: {
    lat: number;
    lng: number;
  }
  markerContent(): string;
  color: string;
}

export class Map {
  private mapboxMap: mapboxgl.Map;

  constructor(divId: string) {
    this.mapboxMap = new mapboxgl.Map({
      container: divId,
      style: 'mapbox://styles/mapbox/streets-v11',
      zoom: 1,
      center: {
        lng: 0,
        lat: 0,
      },
    });
  }

  addMarker(mappable: Mappable) {
    new mapboxgl.Marker({
      color: mappable.color
    }).setLngLat(mappable.location)
      .setPopup(new mapboxgl.Popup({closeButton: false}).setHTML(mappable.markerContent()))
      .addTo(this.mapboxMap);
  }

  // addCompanyMarker(company: Company) {
  //   new mapboxgl.Marker().setLngLat(company.location).addTo(this.mapboxMap);
  // }

  // my idea
//   addMarker(latLngObj: { lat: number; lng: number }, color: string) {
//     new mapboxgl.Marker({
//       color: color,
//     })
//       .setLngLat(latLngObj)
//       .addTo(this.mapboxMap);
//   }
// }