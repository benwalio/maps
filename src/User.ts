import faker from 'faker';
import { Mappable } from './Map';

export class User implements Mappable {
  name: string;
  color: string;
  location: {
    lat: number;
    lng: number;
  };

  constructor(color?: string) {
    this.name = faker.name.firstName();
    this.color = color || 'green';
    this.location = {
      lat: parseFloat(faker.address.latitude()),
      lng: parseFloat(faker.address.longitude()),
    };
  }

  markerContent(): string {
    return `<div><h3>user name: ${this.name}</h3></div>`;
  }
}
