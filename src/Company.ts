import faker from 'faker';
import { Mappable } from './Map';

export class Company implements Mappable {
  name: string;
  catchphrase: string;
  color: string;
  location: {
    lat: number;
    lng: number;
  };

  constructor(color?: string) {
    this.name = faker.company.companyName();
    this.catchphrase = faker.company.catchPhrase();
    this.color = color || 'orange';
    this.location = {
      lat: parseFloat(faker.address.latitude()),
      lng: parseFloat(faker.address.longitude()),
    };
  }

  markerContent(): string {
    return `
    <div>
      <h3>company: ${this.name}</h3>
      <h4>catchphrase: ${this.catchphrase}</h4>
    </div>
      `;
  }
}
